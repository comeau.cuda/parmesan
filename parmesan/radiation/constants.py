# system modules

# internal modules
from parmesan.units import units

# external modules


STEFAN_BOLTZMANN_CONSTANT = (
    5.67037441918442945397099673188923087584012297029130e-8
    * units("W/m^2 / K^4")
)
"""
Stefan-Boltzmann constant

Taken from https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_constant
(24.08.2021)
"""
