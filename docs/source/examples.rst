Examples
========

Here you can find examples for using :mod:`parmesan`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   interactive.rst
   notebooks/physical-calculations
   notebooks/wind-direction
   notebooks/spectrum
   notebooks/spectrum-parseval-theorem
   notebooks/structure
   notebooks/temporal-cycle
