📟 Using PARMESAN interactively
===============================

Handling the units with :mod:`pint`
+++++++++++++++++++++++++++++++++++

:mod:`parmesan` uses :mod:`pint` to handle units. This screencast demonstrates
how useful this can be on its own:

.. asciinema:: asciinema/units.cast
