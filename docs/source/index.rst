Welcome to PARMESAN's documentation!
====================================

:mod:`parmesan` is a Python package for meteorological data analysis.

.. warning::

    Before relying on the output of functions in :mod:`parmesan`, make sure
    they are tested, for example by checking the `Coverage Report
    <coverage-report/index.html>`_.

    The `Coverage Report <coverage-report/index.html>`_ is generated
    automatically from the `test suite
    <https://gitlab.com/tue-umphy/software/parmesan/-/tree/master/tests>`_ in the
    `Pipelines <https://gitlab.com/tue-umphy/software/parmesan/-/pipelines>`_.

    Browse the report for the function you are using and make sure it's marked
    **green** (tested) and not **red** (untested).

    If it is red, consider `writing a test
    <https://gitlab.com/tue-umphy/software/parmesan/-/blob/master/CONTRIBUTING.md#writing-tests>`_
    to make sure it is working as intended.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   settings
   examples
   api/modules
   changelog


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
